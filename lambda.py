import json
import logging
import sys
import  platform

from threading import Timer
import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
max_co2_val = {}
def lambda_handler(event, context):
    global max_co2_val
    #TODO1: Get your data
    co2_val, time_stamp, vehicle = event['message'], event['time_stamp'], event['vehicle']
    # right now vehicle holds a string like vehicle0.csv
    # we need to split the string and return the very last char

    vehicle_val = vehicle.split(".")[0]
    vehicle_val = vehicle_val[len(vehicle_val) - 1]

    #TODO2: Calculate max CO2 emission
    # get the max value from the dictionary
    max_co2 = max_co2_val.get(vehicle_val, 0)
    if co2_val > max_co2:
        max_co2_val[vehicle_val] = co2_val

    #TODO3: Return the result
    print("CO2: {}, Max CO2: {}, Time: {}, Vehicle: {}".format(co2_val, max_co2_val[vehicle_val], time_stamp, vehicle_val))
    
    client.publish(
        topic = "iot/Vehicle_" + vehicle_val,
        queueFullPolicy="AllOrException",
        payload = json.dumps({"co2": co2_val, "max_co2": max_co2_val[vehicle_val], "time_stamp": time_stamp, "vehicle": vehicle_val }),
    )
    
    return
