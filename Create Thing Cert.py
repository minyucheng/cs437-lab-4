################################################### Connecting to AWS
import boto3
import pprint
import json
################################################### Create random name for things
import random
import string

global thingArn
global thingId
global thingName
global defaultPolicyName
thingClient = boto3.client('iot', region_name='us-east-1')
################################################### Parameters for Thing


def createThing(i):
  global thingClient
  thingResponse = thingClient.create_thing(
      thingName = thingName
  )
  data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
  pprint.pprint(data)
  for element in data: 
    if element == 'thingArn':
        thingArn = data['thingArn']
        print(thingArn)
    elif element == 'thingId':
        thingId = data['thingId']
        print(thingId)
    createCertificate(i)
    add_to_group()

def add_to_group():
    client = thingClient
    thing_group_name ='MyGreengrassCoreGroup'
    thing_group_arn ='arn:aws:iot:us-east-1:758401926068:thinggroup/MyGreengrassCoreGroup'
    thing_name = thingName
    thing_arn = thingArn
    client.add_thing_to_thing_group(
        thingGroupName= thing_group_name,
        thingGroupArn= thing_group_arn,
        thingName= thing_name,
        thingArn= thing_arn
    )

def createCertificate(i):
    global thingClient
    certResponse = thingClient.create_keys_and_certificate(
            setAsActive = True
    )
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    for element in data: 
            if element == 'certificateArn':
                    certificateArn = data['certificateArn']
            elif element == 'keyPair':
                    PublicKey = data['keyPair']['PublicKey']
                    PrivateKey = data['keyPair']['PrivateKey']
            elif element == 'certificatePem':
                    certificatePem = data['certificatePem']
            elif element == 'certificateId':
                    certificateId = data['certificateId']        
    with open(f'certificates/device_{i}/device_{i}.public.key', 'w') as outfile:
            outfile.write(PublicKey)
    with open(f'certificates/device_{i}/device_{i}.private.key', 'w') as outfile:
            outfile.write(PrivateKey)
    with open(f'certificates/device_{i}/device_{i}.certificate.pem', 'w') as outfile:
            outfile.write(certificatePem)

    response = thingClient.attach_policy(
            policyName = defaultPolicyName,
            target = certificateArn
    )
    response = thingClient.attach_thing_principal(
            thingName = thingName,
            principal = certificateArn
    )


for j in range(5):
    thingArn = ''
    thingId = ''
    thingName = f'device_{j}'
    defaultPolicyName = 'Test_first_policy'
    thingClient = boto3.client('iot', region_name='us-east-1')
    createThing(j)
    add_to_group()
